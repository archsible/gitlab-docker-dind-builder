==========================================
docker image for gitlab-ci tests
==========================================

.. image:: https://img.shields.io/gitlab/pipeline/archsible/gitlab-docker-dind-builder/master
        :target: https://gitlab.com/archsible/gitlab-docker-dind-builder/pipelines


Docker image with required packages for archsible builds in gitlab-ci

* Free software: MIT license

