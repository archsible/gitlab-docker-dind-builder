FROM docker:latest
RUN apk add --update \
	build-base \
	gcc \
	git \
	libffi-dev \
	openssl \
	openssl-dev \
	python3 \
	python3-dev \
	py3-pip \
	wget
ADD requirements.txt /
RUN pip3 install -r /requirements.txt

ENV PATH /venv/bin:$PATH
